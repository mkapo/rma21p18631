//package ba.etf.rma21.projekat
//
//import ba.etf.rma21.projekat.data.models.Predmet
//import ba.etf.rma21.projekat.data.viewmodels.PredmetListViewModel
//import junit.framework.Assert.assertEquals
//import org.hamcrest.CoreMatchers.not
//import org.junit.Assert.assertArrayEquals
//import org.junit.Assert.assertNotEquals
//import org.junit.Test
//
////OVAJ TEST I TEST KvizViewModelTest NE MOGU SE POKRETATI ZAJEDNO JER SE SNIME PODACI IZ JEDNOG U DRUGI
//
//
//class PredmetViewModelTest {
//    @Test
//    fun getAllSizeTest(){
//        var predmetsViewModel = PredmetListViewModel()
//        assertEquals(predmetsViewModel.getPredmetsAll().size,6)
//    }
//    @Test
//    fun getAllContentTest(){
//        var predmetsViewModel = PredmetListViewModel()
//        var listaPredmeta = listOf("RPR","IM","WT","DM","MLTI","DMINING")
//        var pogresnaListaPredmeta = listOf("RPR","RMA","WT","DM","MLTI","DMINING")
//
//        assertEquals(predmetsViewModel.getPredmetsAll().size,6)
//        assertEquals(predmetsViewModel.getPredmetsAll().map{predmet -> predmet.naziv  },listaPredmeta)
//        assertNotEquals(predmetsViewModel.getPredmetsAll().map{predmet -> predmet.naziv  },pogresnaListaPredmeta)
//    }
//
//    @Test
//    fun getUpisaniAfterAddingTest(){
//        var predmetsViewModel = PredmetListViewModel()
//        predmetsViewModel.refreshUpisani()
//        var listaPredmeta : ArrayList<String> = arrayListOf("MLTI","IM","DM")
//        predmetsViewModel.addPredmetUpisan("IM")
//        predmetsViewModel.addPredmetUpisan("DM")
//
//        assertEquals(predmetsViewModel.getPredmetsUpisani().size,3)
//        assertEquals(predmetsViewModel.getPredmetsUpisani().map{predmet -> predmet.naziv  },listaPredmeta)
//    }
//    @Test
//    fun getPredmetsByGodina(){
//        var predmetsViewModel = PredmetListViewModel()
//        var listaPredmeta : ArrayList<String> = arrayListOf("IM","MLTI")
//
//        assertEquals(predmetsViewModel.getPredmetsByGodina(1).size,2)
//        assertEquals(predmetsViewModel.getPredmetsByGodina(1).map{predmet -> predmet.naziv  },listaPredmeta)
//
//    }
//
//
//}