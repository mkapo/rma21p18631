//package ba.etf.rma21.projekat
//
//import ba.etf.rma21.projekat.data.models.Grupa
//import ba.etf.rma21.projekat.data.viewmodels.KvizListViewModel
//import ba.etf.rma21.projekat.data.viewmodels.PredmetListViewModel
//import junit.framework.Assert.assertEquals
//import org.junit.Test
//
//class KvizViewModelTest {
//    @Test
//    fun getAllTest(){
//        var kvizViewModel = KvizListViewModel()
//        var listaKvizova = listOf("KvizDM2","KvizRPR2","KvizRPR1","KvizDM1","KvizIM2","KvizIM1","KvizMLTI1")
//        assertEquals(kvizViewModel.getAll().map { kviz -> kviz.naziv },listaKvizova)
//    }
//    @Test
//    fun getMyKvizesTest(){
//        var predmetsViewModel = PredmetListViewModel()
//        var kvizViewModel = KvizListViewModel()
//        predmetsViewModel.refreshUpisani()
//        kvizViewModel.refreshMyKvizes()
//
//        var listaKvizova = listOf("KvizMLTI1","KvizIM1")
//
//        predmetsViewModel.addPredmetUpisan("IM")
//        kvizViewModel.addMyKviz(Grupa("Grupa I","IM"))
//
//        assertEquals(listaKvizova, kvizViewModel.getMyKvizes().map { kviz -> kviz.naziv })
//        assertEquals(2, kvizViewModel.getMyKvizes().size)
//
//    }
//    @Test
//    fun getNotTakenTest(){
//        var predmetsViewModel = PredmetListViewModel()
//        var kvizViewModel = KvizListViewModel()
//        predmetsViewModel.refreshUpisani()
//        kvizViewModel.refreshMyKvizes()
//
//        var listaKvizova = listOf("KvizMLTI1","KvizIM1")
//        predmetsViewModel.addPredmetUpisan("IM")
//        kvizViewModel.addMyKviz(Grupa("Grupa I","IM"))
//
//
//        assertEquals(listaKvizova, kvizViewModel.getNotTaken().map { kviz -> kviz.naziv })
//        assertEquals(2, kvizViewModel.getMyKvizes().size)
//    }
//    @Test
//    fun getFutureTest(){
//        var predmetsViewModel = PredmetListViewModel()
//        var kvizViewModel = KvizListViewModel()
//        predmetsViewModel.refreshUpisani()
//        kvizViewModel.refreshMyKvizes()
//
//        var listaKvizova = listOf("KvizDM2")
//
//        predmetsViewModel.addPredmetUpisan("IM")
//        kvizViewModel.addMyKviz(Grupa("Grupa I","IM"))
//        predmetsViewModel.addPredmetUpisan("DM")
//        kvizViewModel.addMyKviz(Grupa("Grupa II","DM"))
//
//        assertEquals(listaKvizova, kvizViewModel.getFuture().map { kviz -> kviz.naziv })
//        assertEquals(1, kvizViewModel.getFuture().size)
//    }
//
//    @Test
//    fun getDone(){
//        var predmetsViewModel = PredmetListViewModel()
//        var kvizViewModel = KvizListViewModel()
//        predmetsViewModel.refreshUpisani()
//        kvizViewModel.refreshMyKvizes()
//
//        var listaKvizova = listOf("KvizRPR1","KvizRPR2")
//
//        predmetsViewModel.addPredmetUpisan("RPR")
//        kvizViewModel.addMyKviz(Grupa("Grupa I","RPR"))
//        predmetsViewModel.addPredmetUpisan("RPR")
//        kvizViewModel.addMyKviz(Grupa("Grupa II","RPR"))
//
//        assertEquals(listaKvizova, kvizViewModel.getDone().map { kviz -> kviz.naziv })
//        assertEquals(2, kvizViewModel.getDone().size)
//    }
//}