//package ba.etf.rma21.projekat
//
//import ba.etf.rma21.projekat.data.viewmodels.GrupaListViewModel
//import ba.etf.rma21.projekat.data.viewmodels.PredmetListViewModel
//import org.junit.Test
//import org.hamcrest.CoreMatchers.hasItem
//import org.hamcrest.MatcherAssert.assertThat
//import org.hamcrest.CoreMatchers.`is` as Is
//import org.hamcrest.core.IsNot.not
//import org.junit.Assert.assertEquals
//
//class GrupaViewModelUnitTest {
//    @Test
//    fun testGrupsByPredmetSize() {
//        var grupsViewModel = GrupaListViewModel()
//        assertEquals(grupsViewModel.getGroupsByPredmet("DM").size,3)
//    }
//    @Test
//    fun testGrupsByPredmetDoesntExist() {
//        var grupsViewModel = GrupaListViewModel()
//        assertEquals(grupsViewModel.getGroupsByPredmet("RMA").size,0)
//    }
//
//}