//package ba.etf.rma21.projekat
//
//
//
//import android.widget.ListView
//import android.widget.Spinner
//import android.widget.TextView
//import androidx.core.view.contains
//import androidx.core.view.get
//import androidx.core.view.size
//import androidx.recyclerview.widget.RecyclerView
//import androidx.test.espresso.Espresso
//import androidx.test.espresso.Espresso.onData
//import androidx.test.espresso.Espresso.onView
//import androidx.test.espresso.action.ViewActions
//import androidx.test.espresso.action.ViewActions.click
//import androidx.test.espresso.assertion.ViewAssertions.matches
//import androidx.test.espresso.contrib.NavigationViewActions
//import androidx.test.espresso.contrib.RecyclerViewActions
//import androidx.test.espresso.matcher.ViewMatchers.*
//import androidx.test.espresso.intent.rule.IntentsTestRule
//import androidx.test.espresso.matcher.ViewMatchers
//import androidx.test.ext.junit.runners.AndroidJUnit4
//import ba.etf.rma21.projekat.data.fragments.FragmentPredmeti
//import ba.etf.rma21.projekat.data.viewmodels.PitanjaKvizViewModel
//import com.google.android.material.navigation.NavigationView
//import org.hamcrest.CoreMatchers
//import org.hamcrest.CoreMatchers.*
//import org.hamcrest.Matchers
//import org.junit.Rule
//import org.junit.Test
//import org.junit.runner.RunWith
//
//@RunWith(AndroidJUnit4::class)
//class MySpirala2AndroidTest {
//
//    @get:Rule
//    val intentsTestRule = IntentsTestRule<MainActivity>(MainActivity::class.java)
//
//    @Test
//    fun upisPredmetDefaultTest () {
//
//        onView(withId(R.id.predmeti)).perform(click())
//        val textGodina = intentsTestRule.activity.findViewById<Spinner>(R.id.odabirGodina).selectedItem.toString()
//        val textPredmet = intentsTestRule.activity.findViewById<Spinner>(R.id.odabirPredmet).selectedItem.toString()
//        val textGrupa = intentsTestRule.activity.findViewById<Spinner>(R.id.odabirGrupa).selectedItem.toString()
//
//
//        onView(withId(R.id.odabirGodina)).perform(click())
//        onView(withText("2")).perform(click());
//        onView(withId(R.id.odabirPredmet)).perform(click())
//        onView(withText("DM")).perform(click());
//        onView(withId(R.id.odabirGrupa)).perform(click())
//        onView(withText("Grupa II")).perform(click());
//
//        onView(withId(R.id.kvizovi)).perform(click())
//        onView(withId(R.id.predmeti)).perform(click())
//
//        //Test reseta na default podatke
//        onView(withId(R.id.odabirGodina)).check(matches(withSpinnerText(containsString(textGodina))))
//        onView(withId(R.id.odabirPredmet)).check(matches(withSpinnerText(containsString(textPredmet))))
//        onView(withId(R.id.odabirGrupa)).check(matches(withSpinnerText(containsString(textGrupa))))
//    }
//    @Test
//    fun upisPredmetOdabir(){
//
//        onView(withId(R.id.predmeti)).perform(click())
//
//        onView(withId(R.id.odabirGodina)).perform(click())
//        onView(withText("2")).perform(click());
//        onView(withId(R.id.odabirPredmet)).perform(click())
//        onView(withText("DM")).perform(click());
//        onView(withId(R.id.odabirGrupa)).perform(click())
//        onView(withText("Grupa II")).perform(click());
//
//
//        onView(withId(R.id.dodajPredmetDugme)).perform(click())
//        onView(withId(R.id.tvPoruka)).check(matches(withText("Uspješno ste upisani u grupu Grupa II predmeta DM!")))
//
//        Espresso.onView(ViewMatchers.withId(R.id.kvizovi)).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.filterKvizova)).perform(ViewActions.click())
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`("Budući kvizovi")
//                )
//        ).perform(ViewActions.click())
//
//        Espresso.onView(ViewMatchers.withId(R.id.listaKvizova)).perform(
//                RecyclerViewActions.scrollTo<RecyclerView.ViewHolder>(
//                        CoreMatchers.allOf(
//                                ViewMatchers.hasDescendant(ViewMatchers.withText("DM"))
//                        )
//                )
//        )
//
//    }
//
//    //testira se tacnost, zaustavljanje kvizva, predavanje kviza, odgovarajuća poruka nakon predavanja kviza
//    //testira se da li se moze odgovor promijeniti
//    @Test
//    fun TestTacnihOdgovora(){
//        val pitanjaKvizViewModel = PitanjaKvizViewModel()
//        //Vraca se sve na default
//        pitanjaKvizViewModel.reset()
//        val pitanja = pitanjaKvizViewModel.getPitanja("KvizMLTI1","MLTI")
//        onView(withId(R.id.listaKvizova)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(hasDescendant(withText("KvizMLTI1")),
//                hasDescendant(withText("MLTI"))),click()))
//        onView(withId(R.id.navigacijaPitanja)).perform(NavigationViewActions.navigateTo(0))
//        onData(anything()).inAdapterView(withId(R.id.odgovoriLista)).atPosition(0).perform(click())
//        //Provjera da li se moze promijeniti rezultat
//        onData(anything()).inAdapterView(withId(R.id.odgovoriLista)).atPosition(2).perform(click())
//        assertThat("Nije ostao isti netacni odgovor",
//                pitanjaKvizViewModel.getTacnost("KvizMLTI1"),
//                CoreMatchers.`is`(Matchers.equalTo(0.0)))
//
//        onView(withId(R.id.zaustaviKviz)).perform(click())
//        onView(withId(R.id.listaKvizova)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(hasDescendant(withText("KvizMLTI1")),
//                hasDescendant(withText("MLTI"))),click()))
//        assertThat("Nije ostalo odgovoreno",
//                pitanjaKvizViewModel.jeLiOdgovoreno("KvizMLTI1",pitanja[0]),
//                notNullValue())
//
//
//        onView(withId(R.id.navigacijaPitanja)).perform(NavigationViewActions.navigateTo(0))
//        //Provjera da li se moze promijeniti rezultat nakon zaustavljanja
//        onData(anything()).inAdapterView(withId(R.id.odgovoriLista)).atPosition(2).perform(click())
//        assertThat("Nije ostao isti netacni odgovor nakon zaustavljanja",
//                pitanjaKvizViewModel.getTacnost("KvizMLTI1"),
//                CoreMatchers.`is`(Matchers.equalTo(0.0)))
//
//        onView(withId(R.id.navigacijaPitanja)).perform(NavigationViewActions.navigateTo(1))
//        onData(anything()).inAdapterView(withId(R.id.odgovoriLista)).atPosition(1).perform(click())
//
//        var staraVelicinaNavigacijePitanja = 0
//        var navigacijaPitanja = intentsTestRule.activity.findViewById<NavigationView>(R.id.navigacijaPitanja)
//        staraVelicinaNavigacijePitanja = navigacijaPitanja.menu.size
//
//        onView(withId(R.id.predajKviz)).perform(click())
//        onView(withId(R.id.tvPoruka)).check(matches(withText("Završili ste kviz KvizMLTI1 sa tačnosti 50.0%")))
//        val tacnost = pitanjaKvizViewModel.getTacnost("KvizMLTI1")
//
//        ViewMatchers.assertThat(
//                "Tacnost nije kakva treba biti",
//                tacnost,
//                CoreMatchers.`is`(Matchers.equalTo(0.5))
//        )
//        onView(withId(R.id.kvizovi)).perform(click())
//        onView(withId(R.id.listaKvizova)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(hasDescendant(withText("KvizMLTI1")),
//                hasDescendant(withText("MLTI"))),click()))
//        navigacijaPitanja = intentsTestRule.activity.findViewById<NavigationView>(R.id.navigacijaPitanja)
//        var novaVelicinaNavigacijePitanja = navigacijaPitanja.menu.size
//        assertThat("Nije se pojavio Rezultat na zadnjem mjestu",
//            staraVelicinaNavigacijePitanja,
//            CoreMatchers.`is`(Matchers.lessThan(novaVelicinaNavigacijePitanja)))
//
//        val menuNavigacije = navigacijaPitanja.menu
//        assert(menuNavigacije.getItem(menuNavigacije.size()-1).title.equals("Rezultat"))
//
//
//        onView(withId(R.id.navigacijaPitanja)).perform(NavigationViewActions.navigateTo(2))
//        onView(withId(R.id.tvPoruka)).check(matches(withText("Završili ste kviz KvizMLTI1 sa tačnosti 50.0%")))
//
//        //Vraca se sve na default
//        pitanjaKvizViewModel.reset()
//    }
//
//
//
//}