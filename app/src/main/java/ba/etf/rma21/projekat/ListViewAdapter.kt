package ba.etf.rma21.projekat

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import ba.etf.rma21.projekat.R
import org.w3c.dom.Text

class ListViewAdapter(context: Context, resource: Int, objects: List<String>) : ArrayAdapter<String>(context, resource, objects) {
    private var zaObojitCrveno : Int = -1
    private var zaObojitZeleno : Int = -1
    private lateinit var odgovor : TextView
    val odgovori = objects
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        if(convertView==null){
            convertView = LayoutInflater.from(context).inflate(R.layout.odgovor,parent,false)
        }
        odgovor = convertView?.findViewById(R.id.tekstOdgovora)!!
        odgovor.text = odgovori.get(position)

        if(zaObojitCrveno==position){
            convertView?.setBackgroundColor(Color.parseColor("#DB4F3D"))
        }
        else if(zaObojitZeleno==position){
            convertView?.setBackgroundColor(Color.parseColor("#3DDC84"))
        }
        else{
            convertView?.setBackgroundColor(Color.TRANSPARENT)
        }

        return convertView
    }
    fun obojiZeleno (position: Int){
        zaObojitZeleno = position
    }
    fun obojiCrveno (position: Int){
        zaObojitCrveno = position
    }
}