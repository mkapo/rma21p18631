package ba.etf.rma21.projekat.data.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.KvizListAdapter
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.viewmodels.DBViewModel
import ba.etf.rma21.projekat.data.viewmodels.KvizListViewModel
import ba.etf.rma21.projekat.data.viewmodels.TakeKvizViewModel
import kotlinx.coroutines.*


class FragmentKvizovi : Fragment() {
    private lateinit var spinner : Spinner
    private lateinit var listaKvizova : RecyclerView
    private lateinit var sortedList : List<Kviz>
    private var dbViewModel = DBViewModel ()
    private var kvizViewModel = KvizListViewModel()
    private lateinit var listaKvizovaAdapter : KvizListAdapter
    val scope = CoroutineScope(
            Job() + Dispatchers.Main)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view =  inflater.inflate(R.layout.kvizes_fragment, container, false)
        spinner = view.findViewById(R.id.filterKvizova)
        activity?.let {
            ArrayAdapter.createFromResource(
                    it,
                R.array.Filters,
                android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
            }
        }
        //dbViewModel.setContext(requireContext())
        kvizViewModel.getAll(onSuccess = ::onSuccess, onError = ::onError)
        listaKvizovaAdapter = KvizListAdapter(arrayListOf(), { kviz -> showKviz(kviz)})

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                val selectedItem = parent.getItemAtPosition(position).toString() //this is your selected item
                promijeniFilter(selectedItem)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        listaKvizova = view.findViewById(R.id.listaKvizova)
        listaKvizova.layoutManager = GridLayoutManager (activity,2, GridLayoutManager.VERTICAL,false)
        listaKvizova.adapter = listaKvizovaAdapter

        return view
    }

    fun onSuccess(kvizes : List<Kviz>){
//        val toast = Toast.makeText(context, "Kvizovi uploadani", Toast.LENGTH_SHORT)
//        toast.show()
        kvizes.sortedWith(compareByDescending({ it.datumPocetka }))
        listaKvizovaAdapter.updateKvizes(kvizes)
    }
    fun onError() {
        listaKvizovaAdapter.updateKvizes(listOf())
    }


    companion object {
        fun newInstance(): FragmentKvizovi = FragmentKvizovi()
    }

    private fun promijeniFilter (selected : String){
        if (selected.equals("Svi moji kvizovi")) {
            listaKvizovaAdapter.promijeniKlikanje(true)
            kvizViewModel.getUpisani(requireContext(), onSuccess = ::onSuccess, onError = ::onError)
        }
        else if(selected.equals("Urađeni kvizovi")){
            listaKvizovaAdapter.promijeniKlikanje(true)
            kvizViewModel.getDone(requireContext(), onSuccess = ::onSuccess, onError = ::onError)
        }
        else if(selected.equals("Budući kvizovi")){
            listaKvizovaAdapter.promijeniKlikanje(true)
            kvizViewModel.getFuture(requireContext(), onSuccess = ::onSuccess, onError = ::onError)
        }
        else if(selected.equals("Prošli kvizovi")){
            listaKvizovaAdapter.promijeniKlikanje(true)
            kvizViewModel.getNotTaken(requireContext(), onSuccess = ::onSuccess, onError = ::onError)
        }
         else{
            listaKvizovaAdapter.promijeniKlikanje(false)
            kvizViewModel.getAll(onSuccess = ::onSuccess, onError = ::onError)
         }
//        listaKvizovaAdapter.updateKvizes(sortedList)
    }
    fun onErrorPokretanjaKviza(){
        val toast = Toast.makeText(context, "Pokretanje pokusaja kviza", Toast.LENGTH_SHORT)
        toast.show()
    }


    private fun showKviz(kviz: Kviz) {
        scope.launch {
            var kvizTaken : KvizTaken? = null
            TakeKvizViewModel().zapocniKviz(requireContext(),
                    kviz.id,
                    {kviz -> kvizTaken = kviz},
                    onError = ::onErrorPokretanjaKviza
            )
            while(kvizTaken==null) { delay(10)}
            if(kvizTaken!!.poruka==null) {
                val fragment = FragmentPokusaj.newInstance(kviz, kvizTaken!!)
                val transaction = requireActivity().supportFragmentManager.beginTransaction()
                var bundle = Bundle()
                bundle.putString("NazivKviza", kviz.naziv)
                fragment.arguments = bundle
                transaction.replace(R.id.fragment_container, fragment)
                transaction.addToBackStack(null)
                transaction.commit()
            }
            else{
                val toast = Toast.makeText(context, kvizTaken!!.poruka, Toast.LENGTH_SHORT)
                toast.show()
            }
        }
    }


}