package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz

@Dao
interface KvizDao {
    @Update
    suspend fun updateKviz(kvizovi : List<Kviz>);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertKviz(kvizovi : List<Kviz>)
    @Delete
    suspend fun deleteKviz(kvizovi : List<Kviz>)
    @Query("SELECT * FROM Kviz")
    suspend fun getAllKviz() : List<Kviz>
    @Query("UPDATE Kviz SET predan=1 WHERE id = :idKviza  ")
    suspend fun predajKviz(idKviza : Int)
    @Query("DELETE FROM Kviz WHERE 1=1")
    suspend fun deleteAll()
}