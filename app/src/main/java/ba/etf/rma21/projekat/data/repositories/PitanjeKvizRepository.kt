package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.api.ApiAdapter
import ba.etf.rma21.projekat.data.models.Pitanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONException
import java.io.IOException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

class PitanjeKvizRepository {

    companion object {
        init {

        }
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }
        suspend fun getPitanja (idKviza : Int) : List<Pitanje>? {
            return withContext(Dispatchers.IO) {
                DBRepository.setContext(context)
                var zavrseno : Boolean? = null
                zavrseno = DBRepository.updateNow()
                while(zavrseno==null) {
                    delay(5)
                }
                //if(zavrseno == false) {
                    val db = AppDatabase.getInstance(context)
                    val pitanja = db.pitanjeDao().getPitanjaWithKviz(idKviza)
                    Log.v("TEXTLOG","SDAD")
                    return@withContext pitanja
                //}
                /*
                var pitanjaResponse = ApiAdapter.retrofit.getPitanjaWithKviz(idKviza)
                var pitanjaResponseBody = pitanjaResponse.body()
                return@withContext pitanjaResponseBody*/
            }
        }
    }
}