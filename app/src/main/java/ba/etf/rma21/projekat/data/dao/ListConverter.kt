package ba.etf.rma21.projekat.data.dao

import androidx.room.TypeConverter
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class ListConverter {
    @TypeConverter
    fun fromList(value : List<String>?) : String? {
        if(value!=null) {
            return value.joinToString(separator = ",")
        }
        return null
    }

    @TypeConverter
    fun toList(value: String?) : List<String>?{
        if(value != null) {
            return value.split(",").map { it }
        }
        return null
    }
}