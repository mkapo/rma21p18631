package ba.etf.rma21.projekat

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.widget.FrameLayout
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.data.fragments.FragmentKvizovi
import ba.etf.rma21.projekat.data.fragments.FragmentPoruka
import ba.etf.rma21.projekat.data.fragments.FragmentPredmeti
import ba.etf.rma21.projekat.data.repositories.AccountRepository
import ba.etf.rma21.projekat.data.viewmodels.AccountViewModel
import ba.etf.rma21.projekat.data.viewmodels.OdgovoriViewModel
//import ba.etf.rma21.projekat.data.fragments.FragmentPredmeti
import ba.etf.rma21.projekat.data.viewmodels.PitanjaKvizViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.*


class MainActivity : AppCompatActivity() {
    var predmet : String = "null"
    var godina : String = "null"
    var grupa : String = "null"
    var back : Boolean = false
    var imeTrenutnogKviza = "null"
    var idTrenutnogKviza : Int? = null
    var brojPitanjaTrenutnogKviza : Double? = null
    var odgovoriViewModel = OdgovoriViewModel()
    var accountViewModel = AccountViewModel()
    val scope = CoroutineScope(
            Job() + Dispatchers.Main)
    private lateinit var bottomNavigation: BottomNavigationView
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.kvizovi -> {
                val kvizesFragment = FragmentKvizovi.newInstance()
                openFragment(kvizesFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.predmeti -> {
                val bundle = Bundle ()
                bundle.putString("Godina", godina)
                bundle.putString("Predmet", predmet)
                bundle.putString("Grupa", grupa)
                val predmetsFragment = FragmentPredmeti.newInstance()
                predmetsFragment.arguments = bundle
                openFragment(predmetsFragment)
                predmet = "null"
                godina = "null"
                grupa = "null"
                return@OnNavigationItemSelectedListener true
            }
            R.id.predajKviz -> {
                val fragment = FragmentPoruka.newInstance()
                val bundle = Bundle()
                val context = applicationContext
                var bodovi : Int? = null
                scope.launch {
                    odgovoriViewModel.predajKviz(context,idTrenutnogKviza!!,{osvojeniBodovi -> bodovi = osvojeniBodovi}, onError = ::onErrorPredaja)
                    while(bodovi == null) {delay (10) }

                    bundle.putString("Poruka", "Završili ste kviz ${imeTrenutnogKviza} sa tačnosti ${bodovi!!.toDouble()*100/(brojPitanjaTrenutnogKviza!!)!!}%")
                    fragment.arguments = bundle
                    openFragment(fragment)
                    bottomNavigation.menu.findItem(R.id.predajKviz).isVisible=false
                    bottomNavigation.menu.findItem(R.id.zaustaviKviz).isVisible=false
                    bottomNavigation.menu.findItem(R.id.kvizovi).isVisible=true
                    bottomNavigation.menu.findItem(R.id.predmeti).isVisible=true
                }
            }
            R.id.zaustaviKviz -> {
                bottomNavigation.selectedItemId= R.id.kvizovi
                bottomNavigation.menu.findItem(R.id.predajKviz).isVisible=false
                bottomNavigation.menu.findItem(R.id.zaustaviKviz).isVisible=false
                bottomNavigation.menu.findItem(R.id.kvizovi).isVisible=true
                bottomNavigation.menu.findItem(R.id.predmeti).isVisible=true

            }
        }
        false
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
    private fun onErrorHash(){

    }
    private fun onErrorPredaja(){

    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val data: Uri? = intent?.data
        Log.i("MyApp", "Deep link clicked " + data);
        val bundle = intent.getStringExtra("payload");
        var uspjesno = false;
        AccountRepository.setContext(applicationContext)
        if(bundle!=null) {
            scope.launch {
                AccountViewModel().postaviHash(bundle,
                        onSuccess = { uspjesnoHash -> uspjesno = uspjesnoHash },
                        onError = ::onErrorHash)
                while (!uspjesno) {
                    delay(10)
                }


                bottomNavigation = findViewById(R.id.bottomNav)
                bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
                bottomNavigation.selectedItemId = R.id.kvizovi
                val fragmentKvizovi = FragmentKvizovi.newInstance()
                openFragment(fragmentKvizovi)
            }
        }
        else{
            bottomNavigation = findViewById(R.id.bottomNav)
            bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
            bottomNavigation.selectedItemId = R.id.kvizovi
            val fragmentKvizovi = FragmentKvizovi.newInstance()
            openFragment(fragmentKvizovi)
        }
    }
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {

        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            val trenutniFragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
            if (trenutniFragment is FragmentPredmeti) {
                predmet = trenutniFragment.view?.findViewById<Spinner>(R.id.odabirPredmet)?.selectedItem.toString()
                godina = trenutniFragment.view?.findViewById<Spinner>(R.id.odabirGodina)?.selectedItem.toString()
                grupa = trenutniFragment.view?.findViewById<Spinner>(R.id.odabirGrupa)?.selectedItem.toString()

                bottomNavigation.selectedItemId=R.id.kvizovi

            }
            if (trenutniFragment is FragmentPoruka) {
                bottomNavigation.selectedItemId=R.id.kvizovi
            }
            true
        } else super.onKeyDown(keyCode, event)
    }
    fun getPodatkeKviza(nazivKviza : String, idKviza : Int, brojPitanja : Int){
        imeTrenutnogKviza = nazivKviza
        idTrenutnogKviza = idKviza
        brojPitanjaTrenutnogKviza = brojPitanja.toDouble()
    }

}

