package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(foreignKeys = arrayOf(
        ForeignKey(onDelete = ForeignKey.CASCADE,entity = Pitanje::class,
                parentColumns = arrayOf("id"),
                childColumns = arrayOf("PitanjeId"))
))
data class Odgovor (
        @PrimaryKey(autoGenerate = true) @SerializedName("id") var id : Int = 0,
        @ColumnInfo (name="odgovoreno") @SerializedName("odgovoreno") var odgovoreno : Int,
        @ColumnInfo (name="PitanjeId") @SerializedName("PitanjeId") var pitanjeId : Int,
        @ColumnInfo (name="KvizId")  var kvizId : Int
        //@SerializedName("message") var poruka : String
)


