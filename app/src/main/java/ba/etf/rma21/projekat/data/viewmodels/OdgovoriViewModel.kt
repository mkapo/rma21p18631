package ba.etf.rma21.projekat.data.viewmodels

import android.content.Context
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.repositories.OdgovorRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import ba.etf.rma21.projekat.data.repositories.TakeKvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class OdgovoriViewModel {
    val scope = CoroutineScope(
            Job() + Dispatchers.Main)
    fun getOdgovoriKviz (context : Context, idKviza : Int, onSuccess: (odgovori: List<Odgovor>?) -> Unit,
                         onError: () -> Unit){
        scope.launch {
            OdgovorRepository.setContext(context)
            val result = OdgovorRepository.getOdgovoriKviz(idKviza)
            when (result!!.size) {
                0 -> onSuccess?.invoke(null)
                else -> onSuccess?.invoke(result)
            }
        }
    }
    fun postaviOdgovorKviz (context : Context, idKvizTaken : Int, idPitanje : Int, odgovor : Int ,onSuccess: (bodovi: Int) -> Unit){
        scope.launch {
            OdgovorRepository.setContext(context)
            val result = OdgovorRepository.postaviOdgovorKviz(idKvizTaken,idPitanje,odgovor)
            when(result) {
                is Int -> onSuccess?.invoke(result)
            }
        }
    }
    fun predajKviz (context: Context, idKviza: Int, onSuccess: (bodovi : Int) -> Unit,
                                                    onError: () -> Unit){
        scope.launch {
            OdgovorRepository.setContext(context)
            val result = OdgovorRepository.predajOdgovoreKviz(idKviza)
            when(result){
                is Int -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }
}