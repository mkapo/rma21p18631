package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.api.ApiAdapter
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class DBRepository {
    companion object {
     init {

     }
        private lateinit var context:Context
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")

        fun setContext(_context:Context){
            context=_context
        }


        suspend fun updateNow(): Boolean? {
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context = context)


                var lastUpdate = getLastUpdate()
                var changed = false
                if(lastUpdate !=null) {
                    var response = ApiAdapter.retrofit.updateNow(AccountRepository.getHash(), lastUpdate)
                    changed = response.body()!!.changed
                }
                if (lastUpdate==null || changed) {
                    db.accountDao().updateLastUpdate(sdf.format(Calendar.getInstance().time),AccountRepository.getHash())
                    val grupe = ApiAdapter.retrofit.getGroupsWithStudent(AccountRepository.getHash()).body()
                    if (grupe != null) {
                        var predmeti = PredmetIGrupaRepository.getPredmeti()
                        if (predmeti != null) {
                            predmeti = predmeti.filter { predmet -> grupe.find { grupa -> grupa.predmetId.equals(predmet.id) } != null }

                            db.predmetDao().insertPredmet(predmeti)
                            val sviPredmetiBaze = getAllPredmet()
                            if (sviPredmetiBaze.size > predmeti.size) {
                                val zaDelete = brisiPredmete(predmeti, sviPredmetiBaze)
                                db.predmetDao().deletePredmet(zaDelete)
                            }

                        db.grupaDao().insertGroup(grupe)
                        val sveGrupeBaze = getAllGroup()
                        if(sveGrupeBaze.size>grupe.size){
                            val zaDelete = brisiGrupe(grupe,sveGrupeBaze)
                            db.grupaDao().deleteGroup(zaDelete)
                        }

                            val listaKvizova = arrayListOf<Kviz>()

                            for(item in grupe){
                                var listaKvizovaBezFK = ApiAdapter.retrofit.getKvizesWithGroup(item.id).body()
                                if(listaKvizovaBezFK!=null){
                                    listaKvizovaBezFK.forEach { kviz -> kviz.GrupaId = item.id; kviz.predan = null }
                                    listaKvizova.addAll(listaKvizovaBezFK!!)
                                }

                            }
                            var kvizovi = listaKvizova.distinct()

                        if(kvizovi.size>0) {

                            val sviKvizoviBaze = getAllKviz()
                            if(sviKvizoviBaze.size>kvizovi.size){
                                val zaDelete = brisiKvizove(kvizovi,sviKvizoviBaze)
                                db.kvizDao().deleteKviz(zaDelete)
                            }

                            val pitanja = ArrayList<Pitanje>()
                            for (item in kvizovi!!) {
                                var pitanjaResponse = ApiAdapter.retrofit.getPitanjaWithKviz(item.id)
                                var pitanjaResponseBody = pitanjaResponse.body()
                                item.pitanja = pitanjaResponseBody!!.map { pitanje -> pitanje.id.toString() }
                                pitanja.addAll(pitanjaResponseBody)
                            }
                            kvizovi = kvizovi.filter { kviz -> sviKvizoviBaze.find {kvizic -> kvizic.id == kviz.id} == null }
                            db.kvizDao().insertKviz(kvizovi)
                            db.pitanjeDao().insertPitanje(pitanja)
                            val svaPitanjaBaze = getAllPitanje()
                            if(svaPitanjaBaze.size>pitanja.size){
                                val zaDelete = brisiPitanja(pitanja,svaPitanjaBaze)
                                db.pitanjeDao().deletePitanje(zaDelete)
                            }
                        }
                        }
                    }
                }

                return@withContext changed
            }
        }
        fun brisiGrupe(grupe : List<Grupa>, sveGrupeBaze : List<Grupa>): ArrayList<Grupa> {
            val zaDeleteGrupe = ArrayList<Grupa>()
            for(item in sveGrupeBaze){
                if(grupe.find { grupa -> grupa.id.equals(item.id) } == null){
                    zaDeleteGrupe.add(item)
                }
            }
            return zaDeleteGrupe
        }
        fun brisiPredmete(predmeti : List<Predmet>, sviPredmetiBaze : List<Predmet>): ArrayList<Predmet> {
            val zaDeletePredmeti = ArrayList<Predmet>()
            for(item in sviPredmetiBaze){
                if(predmeti.find { predmet -> predmet.id.equals(item.id) } == null){
                    zaDeletePredmeti.add(item)
                }
            }
            return zaDeletePredmeti
        }
        fun brisiKvizove(kvizovi : List<Kviz>, sviKvizoviBaze : List<Kviz>): ArrayList<Kviz> {
            val zaDeleteKvizovi = ArrayList<Kviz>()
            for(item in sviKvizoviBaze){
                if(kvizovi.find { kviz -> kviz.id.equals(item.id) } == null){
                    zaDeleteKvizovi.add(item)
                }
            }
            return zaDeleteKvizovi
        }
        fun brisiPitanja(pitanja : List<Pitanje>, svaPitanjaBaze : List<Pitanje>): ArrayList<Pitanje> {
            val zaDeletePitanja = ArrayList<Pitanje>()
            for(item in svaPitanjaBaze){
                if(pitanja.find { pitanje -> pitanje.id.equals(item.id) } == null){
                    zaDeletePitanja.add(item)
                }
            }
            return zaDeletePitanja
        }

        suspend fun getLastUpdate(): String? {
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context = context)
                val account = Account(1,AccountRepository.getHash(),null)
                db!!.accountDao().insertAccount(account)
                val lastUpdate = db!!.accountDao().getLastUpdate()
                return@withContext lastUpdate
            }
        }
        suspend fun getAllGroup() : List<Grupa>{
            return withContext(Dispatchers.IO){
                var db = AppDatabase.getInstance(context = context)
                var grupe = db!!.grupaDao().getAllGroup()
                return@withContext grupe
            }
        }
        suspend fun getAllPredmet() : List<Predmet>{
            return withContext(Dispatchers.IO){
                var db = AppDatabase.getInstance(context = context)
                var predmeti = db!!.predmetDao().getAllPredmet()
                return@withContext predmeti
            }
        }
        suspend fun getAllKviz() : List<Kviz>{
            return withContext(Dispatchers.IO){
                var db = AppDatabase.getInstance(context = context)
                var kvizovi = db!!.kvizDao().getAllKviz()
                return@withContext kvizovi
            }
        }
        suspend fun getAllPitanje() : List<Pitanje>{
            return withContext(Dispatchers.IO){
                var db = AppDatabase.getInstance(context = context)
                var pitanja = db!!.pitanjeDao().getAllPitanje()
                return@withContext pitanja
            }
        }


    }

}