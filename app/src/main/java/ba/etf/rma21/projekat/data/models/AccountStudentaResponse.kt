package ba.etf.rma21.projekat.data.models

import com.google.gson.annotations.SerializedName

data class AccountStudentaResponse (
    @SerializedName ("id") var id : Int,
    @SerializedName ("student") var email : String,
    @SerializedName ("acHash") var hash : String,
    @SerializedName("message") var poruka : String

)