package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.api.Api
import ba.etf.rma21.projekat.data.api.ApiAdapter
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

class PredmetIGrupaRepository {
    companion object {
     init {

     }
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }
        suspend fun getPredmeti(): List<Predmet>? {
            return withContext(Dispatchers.IO) {
                return@withContext ApiAdapter.retrofit.getAllPredmets().body()
            }
        }

        suspend fun getGrupe(): List<Grupa>? {
            return withContext(Dispatchers.IO) {
                return@withContext ApiAdapter.retrofit.getGroups().body()
            }
        }

        suspend fun getGroupsByPredmet(idPredmeta: Int): List<Grupa>? {
            return withContext(Dispatchers.IO) {
                return@withContext ApiAdapter.retrofit.getGroupsWithPredmet(idPredmeta).body()
            }
        }

        suspend fun upisiUGrupu(idGrupa: Int): Boolean? {
            return withContext(Dispatchers.IO) {
                var message = ApiAdapter.retrofit.addStudent(idGrupa, AccountRepository.getHash()).body()
                if (message != null && message.equals("")) {
                    return@withContext true
                }
                return@withContext false
            }
        }

        suspend fun getUpisaneGrupe(): List<Grupa>? {
            return withContext(Dispatchers.IO) {
                DBRepository.setContext(context)
                var zavrseno : Boolean? = null
                zavrseno = DBRepository.updateNow()
                while(zavrseno==null) {
                    delay(10)
                }
                val db = AppDatabase.getInstance(context)
                val grupe = db.grupaDao().getAllGroup()
                return@withContext grupe

                //return@withContext ApiAdapter.retrofit.getGroupsWithStudent(AccountRepository.getHash()).body()
            }
        }

        suspend fun getPredmetById(predmetID : Int): Predmet? {
            return withContext (Dispatchers.IO){
                return@withContext ApiAdapter.retrofit.getPredmetWithID(predmetID).body()
            }
        }
        suspend fun getPredmetsByGodina(godina : Int) : List<Predmet>? {
            return withContext(Dispatchers.IO){
                val predmeti = getPredmeti()
                if(predmeti == null){
                    return@withContext null
                }
                return@withContext predmeti.filter { predmet -> predmet.godina.equals(godina)  }
            }
        }
    }
}