package ba.etf.rma21.projekat.data.fragments

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView.OnItemClickListener
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.ListViewAdapter
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.viewmodels.OdgovoriViewModel
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.*


class FragmentPitanje (pitanje : Pitanje, kvizTaken : KvizTaken) : Fragment() {
    private lateinit var textPitanja : TextView
    private lateinit var odgovori : ListView
    private val odgovoriViewModel = OdgovoriViewModel()
    private var pitanje = pitanje
    private var kvizTaken = kvizTaken
    val scope = CoroutineScope(
            Job() + Dispatchers.Main)
    val scopeTwo = CoroutineScope(
            Job() + Dispatchers.Main)
    fun onErrorDobavljanjaOdgovora(){
        val toast = Toast.makeText(context, "Dobavljanje odgovora", Toast.LENGTH_SHORT)
        toast.show()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.pitanje_fragment, container, false)
        textPitanja = view.findViewById(R.id.tekstPitanja)
        odgovori = view.findViewById(R.id.odgovoriLista)
        var nav = requireActivity().findViewById<NavigationView>(R.id.navigacijaPitanja)


        textPitanja.text = pitanje.tekstPitanja
        var listaOdgovoraPitanja : List<String> = pitanje.opcije
        var tacanOdgovor = pitanje.tacan



        scope.launch {
            var listaSvihOdgovora : List<Odgovor>? = listOf()
            odgovoriViewModel.getOdgovoriKviz(requireContext(),
                    kvizTaken.KvizId,
                    {odgovori -> listaSvihOdgovora = odgovori},
                    onError = ::onErrorDobavljanjaOdgovora
            )
            while(listaSvihOdgovora!=null && listaSvihOdgovora!!.size==0) { delay(10) }
            var arrayAdapter = ListViewAdapter(requireActivity(), android.R.layout.simple_list_item_1, listaOdgovoraPitanja)

            val prijasnjiOdgovorNaPitanje = listaSvihOdgovora?.find { odgovor -> odgovor.pitanjeId.equals(pitanje.id) && odgovor.kvizId.equals(kvizTaken.KvizId)  }

            if (prijasnjiOdgovorNaPitanje != null) {
                arrayAdapter.obojiZeleno(tacanOdgovor)
                if (!prijasnjiOdgovorNaPitanje.odgovoreno.equals(tacanOdgovor)) {

                    arrayAdapter.obojiCrveno(prijasnjiOdgovorNaPitanje.odgovoreno)
                }
                odgovori.isEnabled = false
            }
            odgovori.adapter = arrayAdapter

            var odabraniItemVrijednost = ""
            odgovori.setOnItemClickListener(OnItemClickListener { listView, itemView, itemPosition, itemId ->
                odabraniItemVrijednost = listView.getItemAtPosition(itemPosition).toString()
                scopeTwo.launch {
                    var bodovi : Int? = null
                    odgovoriViewModel.postaviOdgovorKviz(requireContext(),
                            kvizTaken.id,
                            pitanje.id,
                            itemPosition,
                            {osvojeniBodovi -> bodovi=osvojeniBodovi})
                    while(bodovi == null) { delay (10) }
                    kvizTaken.osvojeniBodovi = bodovi!!
                    if (odabraniItemVrijednost.equals(listaOdgovoraPitanja.get(tacanOdgovor))) {
                        itemView.setBackgroundColor(Color.parseColor("#3DDC84"))
                    } else {
                        itemView.setBackgroundColor(Color.parseColor("#DB4F3D"))
                        listView.getChildAt(tacanOdgovor).setBackgroundColor(Color.parseColor("#3DDC84"))
                    }

                }
                odgovori.isEnabled = false
            })
        }
        return view
    }


    companion object{
        fun newInstance (pitanje : Pitanje, kvizTaken : KvizTaken) : FragmentPitanje = FragmentPitanje(pitanje, kvizTaken)
    }
}