package ba.etf.rma21.projekat.data.api

import ba.etf.rma21.projekat.data.models.*
import retrofit2.Response
import retrofit2.http.*

interface Api {

    //UPDATE @Query("date") date : String
    @GET("/account/{id}/lastUpdate")
    suspend fun updateNow(@Path("id") id : String, @Query("date") date : String) : Response<Update>

    //PREDMET

    @GET("/predmet")
    suspend fun getAllPredmets() : Response<List<Predmet>>

    @GET("/predmet/{id}")
    suspend fun getPredmetWithID( @Path("id") id : Int) : Response<Predmet>

    //GRUPA

    @GET("/kviz/{id}/grupa")
    suspend fun getGroupsWithKviz( @Path("id") id : Int) : Response<List<Grupa>>

    @POST("/grupa/{gid}/student/{id}")
    suspend fun addStudent( @Path("gid") gid : Int,
                            @Path("id") id: String) : Response<MessageResponse>

    @GET("/student/{id}/grupa")
    suspend fun getGroupsWithStudent( @Path("id") id : String)
            : Response<List<Grupa>>

    @GET("/grupa")
    suspend fun getGroups() : Response<List<Grupa>>

    @GET("/grupa/{id}")
    suspend fun getGroupWithID ( @Path("id") id : Int) : Response<Grupa>

    @GET("/predmet/{id}/grupa")
    suspend fun getGroupsWithPredmet ( @Path("id") id : Int)
            : Response<List<Grupa>>

    //KVIZ

    @GET("/kviz")
    suspend fun getKvizes () : Response<List<Kviz>>

    @GET("/kviz/{id}")
    suspend fun getKvizWithID ( @Path("id") id : Int) : Response<Kviz>

    @GET("/grupa/{id}/kvizovi")
    suspend fun getKvizesWithGroup( @Path("id") id : Int) : Response<List<Kviz>>

    //ODGOVOR

    @GET("/student/{id}/kviztaken/{ktid}/odgovori")
    suspend fun getOdgovoriWithKvizTaken(@Path("id") id : String,
                                         @Path("ktid") ktid : Int) : Response<List<Odgovor>>

    @POST("/student/{id}/kviztaken/{ktid}/odgovor")
    suspend fun addOdgovoriWithKvizTaken(@Path("id") id : String,
                                         @Path("ktid") ktid : Int, @Body odgovor:OdgovorRequest) : Response<Odgovor>

    //KVIZTAKEN

    @GET("/student/{id}/kviztaken")
    suspend fun getKvizTakensWithStudent ( @Path("id") id : String) : Response<List<KvizTaken>>

    @POST("/student/{id}/kviz/{kid}")
    suspend fun takeKviz ( @Path("id") id : String,
                           @Path("kid") kid : Int) : Response<KvizTaken>

    //ACCOUNT

    @GET("student/{id}")
    suspend fun getStudentWithHash ( @Path("id") id : String) : Response<AccountStudentaResponse>

    @DELETE("/student/{id}/upisugrupepokusaji")
    suspend fun deleteKorisnik ( @Path("id") id : String) : Response<MessageResponse>

    //PITANJE

    @GET("kviz/{id}/pitanja")
    suspend fun getPitanjaWithKviz ( @Path("id") id : Int) : Response<List<Pitanje>>

}