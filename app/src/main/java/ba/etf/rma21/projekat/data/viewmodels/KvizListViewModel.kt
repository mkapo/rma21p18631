package ba.etf.rma21.projekat.data.viewmodels

import android.content.Context
import android.media.Image
import android.util.Log
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.repositories.DBRepository
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import kotlinx.coroutines.*
import java.util.*

class KvizListViewModel{
    val scope = CoroutineScope(
            Job() + Dispatchers.Main)

    fun getAll (onSuccess: (kvizes: List<Kviz>) -> Unit,
                  onError: () -> Unit){
        scope.launch {
            val result = KvizRepository.getAll()
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }

    fun getUpisani (context : Context, onSuccess: (kvizes: List<Kviz>) -> Unit,
                    onError: () -> Unit){
        scope.launch {
            KvizRepository.setContext(context)

            val result = KvizRepository.getUpisani()
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }
    fun getFuture(context : Context, onSuccess: (kvizes: List<Kviz>) -> Unit,
                  onError: () -> Unit){
        scope.launch {
            KvizRepository.setContext(context)
            val result = KvizRepository.getFuture()
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }
    fun getNotTaken(context : Context, onSuccess: (kvizes: List<Kviz>) -> Unit,
                  onError: () -> Unit){
        scope.launch {
            KvizRepository.setContext(context)

            val result = KvizRepository.getNotTaken()
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }
    fun getDone(context : Context, onSuccess: (kvizes: List<Kviz>) -> Unit,
                    onError: () -> Unit){
        scope.launch {
            KvizRepository.setContext(context)

            val result = KvizRepository.getDone()
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }


     fun dajImePredmeta (idKviz: Int, onSuccess: (ime: String) -> Unit,
                                onError: () -> Unit){
        scope.launch {
            val result = KvizRepository.getIme(idKviz)

            when (result) {
                is String -> onSuccess.invoke(result)
                else-> onError.invoke()
            }
        }
    }

    fun postaviSliku (idKviz: Int, onSuccess: (bodovi : Int, chosenDate : Date) -> Unit, onError: () -> Unit){
        scope.launch {
            val result = KvizRepository.postaviSliku(idKviz)

            if(result.first is Int && result.second is Date){
                onSuccess.invoke(result.first!!, result.second!!)
            }
            else if(result.first!=null && result.first==-1){
                onSuccess.invoke(-1, Date(0,0,0))
            }
            else{
                onError.invoke()
            }
        }
    }
}