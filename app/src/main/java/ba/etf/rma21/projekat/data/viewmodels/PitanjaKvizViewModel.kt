package ba.etf.rma21.projekat.data.viewmodels

import android.content.Context
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.DBRepository
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class PitanjaKvizViewModel{
    val scope = CoroutineScope(
            Job() + Dispatchers.Main)

    fun getPitanja(context: Context, idKviza : Int, onSuccess : (pitanja : List<Pitanje>) -> Unit,
                    onError : () -> Unit) {

        scope.launch {
            PitanjeKvizRepository.setContext(context)
            val result = PitanjeKvizRepository.getPitanja(idKviza)
            when (result) {
                is List<Pitanje> -> onSuccess?.invoke(result)
                else -> onError?.invoke()
            }
        }
    }


}