package ba.etf.rma21.projekat.data.viewmodels

import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import ba.etf.rma21.projekat.data.repositories.TakeKvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class TakeKvizViewModel {
    val scope = CoroutineScope(
            Job() + Dispatchers.Main)
    fun zapocniKviz (context : Context, idKviza : Int, onSuccess: (kvizTaken: KvizTaken) -> Unit,
                     onError: () -> Unit){
        scope.launch {
            TakeKvizRepository.setContext(context)
            val result = TakeKvizRepository.zapocniKviz(idKviza)
            when (result) {
                is KvizTaken  -> onSuccess?.invoke(result)
                else -> onError?.invoke()
            }
        }
    }




}