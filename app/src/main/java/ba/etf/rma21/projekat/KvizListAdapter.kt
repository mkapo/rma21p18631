package ba.etf.rma21.projekat

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.viewmodels.KvizListViewModel
import ba.etf.rma21.projekat.data.viewmodels.TakeKvizViewModel
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*

class KvizListAdapter (private var kvizes : List<Kviz>, private val onItemClick: (kviz : Kviz) -> Unit) : RecyclerView.Adapter<KvizListAdapter.KvizViewHolder>(){

    private lateinit var contextAdaptera : Context
    private lateinit var nazivPredmeta : String
    private var dozvoljenoKlikanje = false
    private var kvizListViewModel = KvizListViewModel()
    val scope = CoroutineScope(
            Job() + Dispatchers.Main)
    inner class KvizViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val naziv : TextView = itemView.findViewById(R.id.naziv_kviza)
        val nazivPredmeta : TextView = itemView.findViewById(R.id.naziv_predmeta)
        val datum : TextView = itemView.findViewById(R.id.datum_kviza)
        val trajanje : TextView = itemView.findViewById(R.id.trajanje_kviza)
        val osvojeniBodovi : TextView = itemView.findViewById(R.id.osvojeni_bodovi_kviza)
        val uradjen : ImageView = itemView.findViewById<ImageView>(R.id.uradjen_slika)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KvizViewHolder {
        contextAdaptera = parent.context
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_kviz,parent,false)
         return KvizViewHolder(view)
    }

//    fun dajImePredmeta (idKviza : Int){
//        KvizListViewModel().getGroupsWithKviz(idKviza, onSuccess = ::onSuccess , onError = ::onError)
//    }

//    fun jeliRadjen (idKviza: Int) : KvizTaken? {
//        var radjeniKvizovi = listOf<KvizTaken>()
//        TakeKvizViewModel().getPocetiKvizovi({takens -> radjeniKvizovi = takens}, onError = ::onError)
//        return radjeniKvizovi.find { kvizTaken -> kvizTaken.KvizId.equals(idKviza)}
//    }
//    fun onSuccess (grupe : List<Grupa>){
//        grupeNase = grupe
//        PredmetIGrupaListViewModel().getPredmetById(grupeNase[0].predmetId,{predmet -> nazivPredmeta = predmet.naziv}, onError = ::onError)
//    }

    fun onError() {
        //val toast = Toast.makeText(contextAdaptera, "Error", Toast.LENGTH_SHORT)
        //toast.show()
    }

    override fun getItemCount(): Int = kvizes.size

    override fun onBindViewHolder(holder: KvizViewHolder, position: Int) {
        holder.naziv.text = kvizes[position].naziv
        //dajImePredmeta(kvizes[position].id)
        kvizListViewModel.dajImePredmeta(kvizes[position].id, {ime -> holder.nazivPredmeta.text = ime}, onError = ::onError)
        holder.osvojeniBodovi.text = ""
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val c: Calendar = Calendar.getInstance()
        val currentDate : Date = Date(c[Calendar.YEAR]-1900,c[Calendar.MONTH],c[Calendar.DAY_OF_MONTH])
        var chosenDate : Date = Date(0,0,0)
        val context: Context = holder.uradjen.getContext()
        var id : Int = context.getResources()
                .getIdentifier("plava","drawable",context.packageName)


        if(kvizes[position].datumPocetka.after(currentDate)){
            chosenDate = kvizes[position].datumPocetka
                id = context.getResources()
                    .getIdentifier("zuta","drawable",context.packageName)
        }
        else if(kvizes[position].datumPocetka.before(currentDate) && (kvizes[position].datumKraj==null || kvizes[position].datumKraj!!.after(currentDate))){
            if(kvizes[position].datumKraj!=null) {
                chosenDate = kvizes[position].datumKraj!!
            }
            else{
                chosenDate = kvizes[position].datumPocetka
            }

            id = context.getResources()
                    .getIdentifier("zelena","drawable",context.packageName)
        }

        else {
            id = context.getResources()
                            .getIdentifier("crvena", "drawable", context.packageName)
            chosenDate = kvizes[position].datumKraj!!
        }
        holder.uradjen.setImageResource(id)
        scope.launch {
            var bodoviIzCoroutine : Int? = null
            var dateIzCoroutine : Date? = null
            kvizListViewModel.postaviSliku(kvizes[position].id,
                    { bodovi, chosen ->
                        bodoviIzCoroutine = bodovi;
                        dateIzCoroutine = chosen
                    }, onError = ::onError)
            while(bodoviIzCoroutine==null) { delay(10)}
            if(!bodoviIzCoroutine!!.equals(-1)){
                id = context.getResources()
                        .getIdentifier("plava", "drawable", context.packageName)
                holder.uradjen.setImageResource(id);
                holder.osvojeniBodovi.text = bodoviIzCoroutine.toString();
                chosenDate = dateIzCoroutine!!
            }

            var formatDatuma = SimpleDateFormat("dd.MM.yyyy")
            holder.datum.text = formatDatuma.format(chosenDate).toString()

            holder.trajanje.text = kvizes[position].trajanje.toString() + " min"
            holder.itemView.setOnClickListener() {
                if(dozvoljenoKlikanje) {
                    onItemClick(kvizes[position])
                }
                else{
                    val toast = Toast.makeText(context, "Ne mozete pokrenuti kviz iz ovog filtera", Toast.LENGTH_SHORT)
                    toast.show()
                }
            }
        }
    }

    fun updateKvizes(sortedList: List<Kviz>) {
        this.kvizes=sortedList
        notifyDataSetChanged()
    }
    fun promijeniKlikanje(klikanje : Boolean){
        dozvoljenoKlikanje = klikanje
    }
}