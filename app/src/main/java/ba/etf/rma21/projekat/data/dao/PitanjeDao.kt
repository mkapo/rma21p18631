package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Pitanje

@Dao
interface PitanjeDao {
    @Update
    suspend fun updatePitanje(pitanja : List<Pitanje>);
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertPitanje(pitanja : List<Pitanje>)
    @Delete
    suspend fun deletePitanje(pitanje : List<Pitanje>)
    @Query("SELECT * FROM Pitanje")
    suspend fun getAllPitanje() : List<Pitanje>
    @Query ("SELECT * FROM Pitanje WHERE (SELECT k.pitanja FROM Kviz k WHERE k.id=:idKviza) LIKE ('%' || CAST(id as varchar(2)) || '%' ) ")
    suspend fun getPitanjaWithKviz (idKviza : Int) : List<Pitanje>
    @Query ("SELECT * FROM Pitanje WHERE id = :idPitanja")
    suspend fun getPitanjeWithId (idPitanja : Int) : Pitanje
    @Query("DELETE FROM Pitanje WHERE 1=1")
    suspend fun deleteAll()
}