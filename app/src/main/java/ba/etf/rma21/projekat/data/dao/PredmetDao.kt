package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet

@Dao
interface PredmetDao {
    @Update
    suspend fun updatePredmet(predmeti : List<Predmet>);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPredmet(predmeti : List<Predmet>)
    @Delete
    suspend fun deletePredmet(predmeti : List<Predmet>)
    @Query("SELECT * FROM Predmet")
    suspend fun getAllPredmet() : List<Predmet>
    @Query("DELETE FROM Predmet WHERE 1=1")
    suspend fun deleteAll()
}