package ba.etf.rma21.projekat.data.dao

import androidx.room.TypeConverter
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class DateConverter {
    @TypeConverter
    fun toDate(value : String?) : Date? {
        if(value != null){
            var simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss")
            var date = Date(value)
            val formatiranDate = simpleDateFormat.format(date)
            val dateString = simpleDateFormat.parse(formatiranDate)
            return dateString
        }
        return null
    }

    @TypeConverter
    fun fromDate(date : Date?) : String? {
        if(date!=null){
            return date.toString()
        }
        return null
    }



}