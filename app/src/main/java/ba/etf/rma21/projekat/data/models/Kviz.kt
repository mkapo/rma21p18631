package ba.etf.rma21.projekat.data.models

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import ba.etf.rma21.projekat.data.dao.DateConverter
import ba.etf.rma21.projekat.data.dao.ListConverter
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
@TypeConverters(DateConverter::class,ListConverter::class)

data class Kviz(
        @PrimaryKey @SerializedName("id") var id : Int,
        @ColumnInfo (name="naziv") @SerializedName("naziv") var naziv : String,
        @ColumnInfo (name="datumPocetka") @SerializedName("datumPocetak") var datumPocetka : Date,
        @ColumnInfo (name="datumKraj") @SerializedName("datumKraj") var datumKraj : Date?,
        @ColumnInfo (name="trajanje") @SerializedName("trajanje") var trajanje : Int,
        @ColumnInfo (name="GrupaId") var GrupaId : Int,
        @ColumnInfo (name="predan") var predan : Boolean?,
        @ColumnInfo (name="pitanja") var pitanja : List<String>
        //@SerializedName("message") var poruka : String
)