package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Grupa(
        @PrimaryKey @SerializedName("id") var id : Int,
        @ColumnInfo(name="naziv") @SerializedName("naziv") var naziv : String,
        @ColumnInfo(name="PredmetId") @SerializedName("PredmetId") var predmetId : Int
        //@SerializedName("message") var poruka : String
)
