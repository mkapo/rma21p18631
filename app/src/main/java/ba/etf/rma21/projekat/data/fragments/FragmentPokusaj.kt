package ba.etf.rma21.projekat.data.fragments

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.viewmodels.PitanjaKvizViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.*

class FragmentPokusaj(kviz : Kviz, kvizTaken : KvizTaken) : Fragment(){
    private lateinit var bottomNav : BottomNavigationView
    private lateinit var leftNav : NavigationView
    private val kviz = kviz
    private lateinit var menu : Menu
    private var pitanjaKvizViewModel = PitanjaKvizViewModel()
    private var nazivKviza = ""
    var listaPitanja = listOf<Pitanje>()
    val scope = CoroutineScope(
            Job() + Dispatchers.Main)
    private var mOnNavigationItemSelectedListener = NavigationView.OnNavigationItemSelectedListener { item ->


        if(item.title.equals("Rezultat")){
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            val fragment = FragmentPoruka.newInstance()
            val bundle = Bundle()
            bottomNav.menu.findItem(R.id.kvizovi).isVisible = true
            bottomNav.menu.findItem(R.id.predmeti).isVisible = true
            bottomNav.menu.findItem(R.id.predajKviz).isVisible = false
            bottomNav.menu.findItem(R.id.zaustaviKviz).isVisible = false
            var brojPitanja = 1.0
            if(listaPitanja.size!=0){
                brojPitanja = listaPitanja.size.toDouble()
            }
            var osvojeniBodovi = 0.0
            if(kvizTaken.osvojeniBodovi!=null){
                osvojeniBodovi = kvizTaken.osvojeniBodovi.toDouble()
            }
            bundle.putString("Poruka", "Tačnost na kvizu ${nazivKviza} je ${osvojeniBodovi*100/(brojPitanja)}%")
            fragment.arguments = bundle
            transaction.replace(R.id.fragment_container, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        else {
            val transaction = childFragmentManager.beginTransaction()
            scope.launch {
                var pitanje : Pitanje? = listaPitanja.get((Integer.parseInt(item.title as String)) - 1)
//                PitanjaKvizViewModel().getPitanja(requireContext(),
//                        kviz.id,
//                        {pitanjca -> pitanje = pitanjca.get((Integer.parseInt(item.title as String)) - 1)},
//                        onError = ::onErrorSlanjePitanja
//                )
//                while(pitanje==null) { delay(10)}
                val fragment = FragmentPitanje(pitanje!!, kvizTaken)
                transaction.replace(R.id.framePitanje, fragment)
                transaction.addToBackStack(null)
                transaction.commit()
            }
        }
        true
    }
    fun onErrorSlanjePitanja(){
        val toast = Toast.makeText(context, "Slanje pitanja", Toast.LENGTH_SHORT)
        toast.show()
    }
    fun onErrorDobavljanjePitanja(){
        val toast = Toast.makeText(context, "Dobavljanje pitanja", Toast.LENGTH_SHORT)
        toast.show()
    }





    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.pokusaj_fragment,container,false)
        bottomNav = activity?.findViewById(R.id.bottomNav)!!
        nazivKviza = kviz.naziv
        //pitanjaKvizViewModel.addPitanjaKvizu(nazivKviza, listaPitanja as ArrayList<Pitanje>)
        leftNav = view.findViewById(R.id.navigacijaPitanja)
        menu = leftNav.menu

        scope.launch {
            PitanjaKvizViewModel().getPitanja(requireContext(),
                    kviz.id,
                    {pitanja -> listaPitanja = pitanja},
                    onError = ::onErrorDobavljanjePitanja
            )
            while(listaPitanja.size==0) { delay(10) }

            for (i in 0..listaPitanja.size - 1) {
                menu.add(R.id.grupaBrojeva, i, Menu.NONE, (i + 1).toString())
            }
            (activity as MainActivity).getPodatkeKviza(kviz.naziv,kviz.id,listaPitanja.size)

            bottomNav.menu.findItem(R.id.kvizovi).isVisible = false
            bottomNav.menu.findItem(R.id.predmeti).isVisible = false
            bottomNav.menu.findItem(R.id.zaustaviKviz).isVisible = true

            if(kviz.predan!=null && kviz.predan!!) {
                bottomNav.menu.findItem(R.id.predajKviz).isVisible = false
                menu.add(R.id.grupaBrojeva, menu.size(), Menu.NONE, "Rezultat")
            }
            else{
                bottomNav.menu.findItem(R.id.predajKviz).isVisible = true
            }

            leftNav.setNavigationItemSelectedListener(mOnNavigationItemSelectedListener)


        }
        return view
    }

    companion object{
        fun newInstance (kviz: Kviz, kvizTaken: KvizTaken) : FragmentPokusaj = FragmentPokusaj(kviz, kvizTaken)
    }
}