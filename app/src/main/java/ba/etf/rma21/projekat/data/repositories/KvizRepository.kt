package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.api.ApiAdapter
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import java.util.*

class KvizRepository {

    companion object {


        init {

        }
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun getAll() : List<Kviz>?{
            return withContext(Dispatchers.IO){
                var responseLista = ApiAdapter.retrofit.getKvizes()
                var lista = responseLista?.body()
                return@withContext lista
            }
        }

        suspend fun getById(id:Int):Kviz? {
            return withContext(Dispatchers.IO){
                val response = ApiAdapter.retrofit.getKvizWithID(id).body()
                return@withContext response
            }
        }
        suspend fun getUpisani():List<Kviz>? {
            return withContext(Dispatchers.IO){
                DBRepository.setContext(context)
                var zavrseno : Boolean? = null
                zavrseno = DBRepository.updateNow()
                while(zavrseno==null) {delay(10)}

                    val db = AppDatabase.getInstance(context)
                    val upisaniKvizovi = db.kvizDao().getAllKviz()
                    DBRepository.setContext(context)
                    return@withContext upisaniKvizovi

            }
        }
        suspend fun getFuture():List<Kviz>? {
            return withContext(Dispatchers.IO){
                val c: Calendar = Calendar.getInstance()
                val currentDate : Date = Date(c[Calendar.YEAR]-1900,c[Calendar.MONTH],c[Calendar.DAY_OF_MONTH])
                return@withContext getUpisani()?.filter { kviz -> kviz.datumKraj!=null && kviz.datumKraj!!.after(currentDate)}
            }
        }
        suspend fun getNotTaken(): List<Kviz>? {
            return withContext(Dispatchers.IO){
                val c: Calendar = Calendar.getInstance()
                val currentDate : Date = Date(c[Calendar.YEAR]-1900,c[Calendar.MONTH],c[Calendar.DAY_OF_MONTH])
                if(TakeKvizRepository.getPocetiKvizovi() ==null){
                    return@withContext null
                }
                val kvizesTaken = TakeKvizRepository.getPocetiKvizovi()
                if(kvizesTaken==null){
                    return@withContext null
                }
                return@withContext getUpisani()?.
                    filter { kviz -> kviz.datumKraj!=null &&
                                     kviz.datumKraj!!.after(currentDate) &&
                                     kvizesTaken.find { kvizTaken -> kvizTaken.KvizId.equals(kviz.id) }==null}
            }
        }
        suspend fun getDone(): List<Kviz>? {
            return withContext(Dispatchers.IO){
                val c: Calendar = Calendar.getInstance()
                val currentDate : Date = Date(c[Calendar.YEAR]-1900,c[Calendar.MONTH],c[Calendar.DAY_OF_MONTH])
                val kvizesTaken = TakeKvizRepository.getPocetiKvizovi()
                if(kvizesTaken==null){
                    return@withContext null
                }
                return@withContext getUpisani()?.
                filter { kviz ->
                        kvizesTaken.find { kvizTaken -> kvizTaken.KvizId.equals(kviz.id) }!=null}
            }
        }

        suspend fun getGroupsWithKviz (idKviza : Int) : List<Grupa>?{
            return withContext(Dispatchers.IO){
                DBRepository.setContext(context)
                var zavrseno : Boolean? = null
                zavrseno = DBRepository.updateNow()
                while(zavrseno==null) {delay(10)}
                //if(zavrseno==false) {
                    var db = AppDatabase.getInstance(context)
                    var upisaniKvizovi = db.grupaDao().getGroupsWithKviz(idKviza)
                    return@withContext upisaniKvizovi
               //}
                /*
                val grupe = ApiAdapter.retrofit.getGroupsWithKviz(idKviza)
                val grupebody = grupe.body()
                return@withContext grupebody*/
            }
        }
        suspend fun getIme (idKviza : Int) : String?{
            return withContext(Dispatchers.IO){
                val grupe = ApiAdapter.retrofit.getGroupsWithKviz(idKviza)
                val grupebody = grupe.body()
                if(grupebody==null){
                    return@withContext null
                }
                val predmet = ApiAdapter.retrofit.getPredmetWithID(grupebody.get(0).predmetId).body()
                if(predmet==null){
                    return@withContext null
                }
                return@withContext predmet.naziv
            }
        }

        suspend fun postaviSliku(idKviz: Int): Pair<Int?,Date?> {
            return withContext(Dispatchers.IO){
                val kvizesTaken = ApiAdapter.retrofit.getKvizTakensWithStudent(AccountRepository.getHash()).body()
                if(kvizesTaken==null){
                    return@withContext Pair(-1,null)
                }
                val kviz = kvizesTaken.find {kvizTaken -> kvizTaken.KvizId.equals(idKviz) }
                if(kviz==null){
                    return@withContext Pair(-1,null)
                }


                return@withContext Pair(kviz.osvojeniBodovi,kviz.datumRada)

            }

        }
    }
}