package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Account
import java.util.*

@Dao
interface AccountDao {
    @Query("SELECT lastUpdate FROM Account WHERE id= 1")
    suspend fun getLastUpdate() : String

    @Query("UPDATE Account SET lastUpdate = :newUpdate, acHash = :acHashic WHERE id= 1")
    suspend fun updateLastUpdate(newUpdate : String, acHashic : String)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAccount (account: Account)
}