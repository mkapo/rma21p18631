package ba.etf.rma21.projekat.data.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R
import java.util.zip.Inflater

class FragmentPoruka : Fragment() {
    private lateinit var text : TextView
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.poruka_fragment, container, false)
        text = view.findViewById(R.id.tvPoruka)
        val bundle = this.arguments
        if (bundle != null) {
            text.text=bundle.getString("Poruka")
        }
        return view
    }

    companion object {
        fun newInstance() : FragmentPoruka = FragmentPoruka()
    }
}