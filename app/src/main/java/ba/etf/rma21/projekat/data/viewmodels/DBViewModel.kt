package ba.etf.rma21.projekat.data.viewmodels

import android.content.Context
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.DBRepository
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class DBViewModel {
    val scope = CoroutineScope(
        Job() + Dispatchers.Main)

    fun setContext (context : Context){
        DBRepository.setContext(context)
    }
    fun updateNow (onSuccess: (zavrseno: Boolean) -> Unit){
        scope.launch {
            val result = DBRepository.updateNow()
            when (result){
                is Boolean -> onSuccess.invoke(result)
            }
        }
    }
}