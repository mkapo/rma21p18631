package ba.etf.rma21.projekat.data.models

import androidx.room.*
import ba.etf.rma21.projekat.data.dao.ListConverter
import com.google.gson.annotations.SerializedName
import org.json.JSONArray

@Entity
@TypeConverters(ListConverter::class)
data class Pitanje(
    @PrimaryKey @SerializedName("id") val id: Int,
    @ColumnInfo (name="naziv") @SerializedName("naziv") val naziv: String,
    @ColumnInfo (name="tekstPitanja") @SerializedName("tekstPitanja") val tekstPitanja: String,
    @ColumnInfo (name="opcije") @SerializedName("opcije") val opcije: List<String>,
    @ColumnInfo (name="tacan") @SerializedName("tacan") val tacan: Int
    //@ColumnInfo (name="KvizId") var KvizId : Int
    //@SerializedName("message") var poruka : String
)