package ba.etf.rma21.projekat.data.viewmodels

import ba.etf.rma21.projekat.data.repositories.AccountRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class AccountViewModel {
    val scope = CoroutineScope(
        Job() + Dispatchers.Main)

    fun postaviHash (acHash : String, onSuccess : (uspjesno : Boolean) -> Unit,
                        onError : () -> Unit){

        scope.launch{
            val result = AccountRepository.postaviHash(acHash)
            when(result){
                true -> onSuccess?.invoke(result)
                false -> onError?.invoke()
            }
        }
    }
    fun getHash (onSuccess : (uspjesno : String) -> Unit,
                 onError : () -> Unit){

        scope.launch{
            var result = AccountRepository.getHash()
            when(result){
                is String -> onSuccess?.invoke(result)
                else -> onError?.invoke()
            }
        }
    }
}