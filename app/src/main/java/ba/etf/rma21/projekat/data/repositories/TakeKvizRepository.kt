package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.api.ApiAdapter
import ba.etf.rma21.projekat.data.models.KvizTaken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

class TakeKvizRepository {
    companion object {
     init {

     }
        private lateinit var context : Context
        fun setContext (contextApp : Context){
            context = contextApp
        }

        suspend fun zapocniKviz(idKviza: Int): KvizTaken? {
            return withContext(Dispatchers.IO) {
                if(getPocetiKvizovi()!=null) {
                    val kvizTakeno =
                        getPocetiKvizovi()!!.find { kvizTaken -> kvizTaken.KvizId.equals(idKviza) }
                    if (kvizTakeno != null) {
                        Log.v("TEXT","SDA")
                        return@withContext kvizTakeno
                    }
                }


                val kvizTakenResponse = ApiAdapter.retrofit.takeKviz(AccountRepository.getHash(), idKviza)
                var kvizTaken = kvizTakenResponse.body()
                kvizTaken!!.KvizId = idKviza
                var db = AppDatabase.getInstance(context)
                db.odgovorDao().deleteOdgovorWithKviz(idKviza)
                return@withContext kvizTaken

            }
        }

        suspend fun getPocetiKvizovi(): List<KvizTaken>? {
            return withContext(Dispatchers.IO) {
                val pokusajiResponse = ApiAdapter.retrofit.getKvizTakensWithStudent(AccountRepository.getHash())
                val pokusaji = pokusajiResponse.body()
                if(pokusaji!!.size==0){
                    return@withContext null
                }
                return@withContext pokusaji

            }
        }
    }
}