package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.api.ApiAdapter
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.OdgovorRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class OdgovorRepository {
    companion object {
     init {

     }
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }
        suspend fun getOdgovoriKviz(idKviza: Int): List<Odgovor>? {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                var odgovori: List<Odgovor>? = listOf()

                odgovori = db.odgovorDao().getOdgovorsWithKviz(idKviza)

                return@withContext odgovori
            }
        }

        suspend fun postaviOdgovorKvizWebServis(idKvizTaken: Int, idPitanje: Int, odgovor: Int): Int? {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                var pitanje = db.pitanjeDao().getPitanjeWithId(idPitanje)
                var listaKvizTakenResponse = ApiAdapter.retrofit.getKvizTakensWithStudent(AccountRepository.getHash())
                var trenutniBodovi: Int? = null
                if(listaKvizTakenResponse.body()==null){
                    return@withContext null
                }

                var kvizTaken = listaKvizTakenResponse.body()!!.find { kvizTaken -> kvizTaken.id.equals(idKvizTaken) }
                trenutniBodovi = kvizTaken!!.osvojeniBodovi
                if (pitanje.tacan.equals(odgovor)) {
                    trenutniBodovi = trenutniBodovi + 1
                }

                var odgovorResult = trenutniBodovi.let { OdgovorRequest(odgovor, idPitanje, it) }
                var response = ApiAdapter.retrofit.addOdgovoriWithKvizTaken(AccountRepository.getHash(), idKvizTaken, odgovorResult)

                return@withContext trenutniBodovi
            }
        }
        suspend fun postaviOdgovorKviz(idKvizTaken: Int, idPitanje: Int, odgovor: Int): Int? {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                var pitanje = db.pitanjeDao().getPitanjeWithId(idPitanje)

                var listaKvizTakenResponse = ApiAdapter.retrofit.getKvizTakensWithStudent(AccountRepository.getHash())
                var trenutniBodovi: Int? = null
                if(listaKvizTakenResponse.body()==null){
                    return@withContext null
                }
                var kvizTaken = listaKvizTakenResponse.body()!!.find { kvizTaken -> kvizTaken.id.equals(idKvizTaken) }
                var brojPitanja : Double = db.pitanjeDao().getPitanjaWithKviz(kvizTaken!!.KvizId).size.toDouble()
                trenutniBodovi = kvizTaken!!.osvojeniBodovi
                if (pitanje.tacan.equals(odgovor)) {
                    trenutniBodovi = trenutniBodovi + 1
                }
                if(db.odgovorDao().getOdgovorWithKvizAndPitanje(idPitanje,kvizTaken.KvizId).size==0){
                    db.odgovorDao().insertOdgovor(Odgovor(0, odgovor, idPitanje, kvizTaken.KvizId))
                }
                return@withContext (trenutniBodovi.toDouble()*100/brojPitanja).toInt()
            }
        }
        suspend fun predajOdgovoreKviz(idKviz : Int) : Int{
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                val odgovori = db.odgovorDao().getOdgovorsWithKviz(idKviz)
                var kviz = db.kvizDao().predajKviz(idKviz)
                var listaKvizTakenResponse = ApiAdapter.retrofit.getKvizTakensWithStudent(AccountRepository.getHash())
                var kvizTaken = listaKvizTakenResponse.body()!!.find{kvizTaken -> kvizTaken.KvizId.equals(idKviz)}
                var sumaBodova = 0
                for(item in odgovori){
                    sumaBodova = postaviOdgovorKvizWebServis(kvizTaken!!.id,item.pitanjeId,item.odgovoreno)!!
                }
                return@withContext sumaBodova
            }
        }
    }

}