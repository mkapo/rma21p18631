package ba.etf.rma21.projekat.data.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.viewmodels.KvizListViewModel
import ba.etf.rma21.projekat.data.viewmodels.PredmetIGrupaListViewModel
import kotlinx.coroutines.*

class FragmentPredmeti : Fragment(){
    private lateinit var odabirGodina : Spinner
    private lateinit var odabirPredmet : Spinner
    private lateinit var odabirGrupa : Spinner
    private lateinit var upisDugme : Button
    private var predmeti = PredmetIGrupaListViewModel()
    private var godine = arrayOf("1","2","3","4","5")
    private var kvizList = KvizListViewModel()
    private var odabraniIdGrupe : Int? = null
    private var listaPredmeta = listOf<Predmet>()
    private var listaGrupa = listOf<Grupa> ()
    val scope = CoroutineScope(
            Job() + Dispatchers.Main)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view =  inflater.inflate(R.layout.predmets_fragment, container, false)
        odabirGodina = view.findViewById(R.id.odabirGodina)
        odabirPredmet = view.findViewById(R.id.odabirPredmet)
        odabirGrupa = view.findViewById(R.id.odabirGrupa)
        upisDugme = view.findViewById(R.id.dodajPredmetDugme)
        var bundle = this.arguments
            var spinnerAdapterGodine = activity?.let {
                ArrayAdapter(
                        it,
                        android.R.layout.simple_spinner_item
                        ,godine
                ).also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    odabirGodina.adapter = adapter
                }
            }
        var prethodnaGodina = bundle?.getString("Godina")
        var pozicijaGodine = 0
        for(i in 0..odabirGodina.adapter.count-1){
            if(odabirGodina.adapter.getItem(i).toString().equals(prethodnaGodina.toString())){
                pozicijaGodine=i
                break
            }
        }
        odabirGodina.setSelection(pozicijaGodine)



        promijeniFilterPredmeta(odabirGodina.selectedItem.toString(), bundle)

            odabirGodina.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                    val selectedItem = parent.getItemAtPosition(position).toString() //this is your selected item

                    promijeniFilterPredmeta(selectedItem,bundle)


                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
            odabirPredmet.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                    val selectedItem = listaPredmeta.get(position).id

                    promijeniFilterGrupa(selectedItem,bundle)


                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }

        upisDugme.setOnClickListener(){
            posaljiPodatke()
        }
        return view
    }

    companion object {
        fun newInstance(): FragmentPredmeti = FragmentPredmeti()
    }


    fun onErrorPredmeti(){
        val toast = Toast.makeText(context, "Ta godina nema predmeta", Toast.LENGTH_SHORT)
        toast.show()
    }


    private fun promijeniFilterPredmeta(selectedItem: String, bundle : Bundle?) {
        listaPredmeta = listOf()
        scope.launch {
            predmeti.getPredmetsByGodina(selectedItem.toInt(),
                    onSuccess = { predmeti -> listaPredmeta = predmeti }
                    , onError = ::onErrorPredmeti)
            while(listaPredmeta.size==0){
                delay(10)
            }

                    var spinnerAdapterPredmeti = activity?.let {
                        ArrayAdapter(
                                it,
                                android.R.layout.simple_spinner_item
                                ,listaPredmeta.map{ predmet -> predmet.naziv }
                        ).also { adapter ->
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                            odabirPredmet.adapter = adapter
                        }
                    }
                    var pozicijaPredmeta  = 0
                    if(bundle!=null) {
                        var prethodniPredmeti = bundle?.getString("Predmet")

                        for (i in 0..odabirPredmet.adapter.count - 1) {
                            if (odabirPredmet.adapter.getItem(i).toString().equals(prethodniPredmeti.toString())) {
                                pozicijaPredmeta = i
                                break
                            }
                        }
                    }
                    odabirPredmet.setSelection(pozicijaPredmeta)
                    if(odabirPredmet.selectedItem!=null) {
                        promijeniFilterGrupa(listaPredmeta.get(pozicijaPredmeta).id, bundle)
                    }
                }


        }


    fun onErrorGrupe(){
        val toast = Toast.makeText(context, "Taj predmet nema grupa", Toast.LENGTH_SHORT)
        toast.show()
    }


    private fun promijeniFilterGrupa(idPredmeta: Int, bundle: Bundle?) {
        listaGrupa = listOf()
        scope.launch {
            PredmetIGrupaListViewModel().getGroupsByPredmet(idPredmeta,
                    { grupe -> listaGrupa = grupe },
                    onError = ::onErrorGrupe)
            while(listaGrupa.size==0){
                delay(10)
            }

                    var spinnerAdapterGrupe = activity?.let {
                        ArrayAdapter(
                                it,
                                android.R.layout.simple_spinner_item
                                , listaGrupa.map { grupa -> grupa.naziv }
                        ).also { adapter ->
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                            odabirGrupa.adapter = adapter
                        }
                    }
                    var pozicijaGrupe = 0
                    if(bundle!=null) {
                        var prethodnaGrupa = bundle?.getString("Grupa")


                        for (i in 0..odabirGrupa.adapter.count - 1) {
                            if (odabirGrupa.adapter.getItem(i).toString().equals(prethodnaGrupa.toString())) {
                                pozicijaGrupe = i
                                break
                            }
                        }
                    }
                    odabraniIdGrupe = listaGrupa.get(pozicijaGrupe).id
                    odabirGrupa.setSelection(pozicijaGrupe)
        }
    }



    fun onUpisError (){
        val toast = Toast.makeText(context, "Upis nije uspio", Toast.LENGTH_SHORT)
        toast.show()
    }

    private fun posaljiPodatke() {
        //predmeti.addPredmetUpisan(odabirPredmet.selectedItem.toString())
        //kvizList.addMyKviz(Grupa(odabirGrupa.selectedItem.toString(),odabirPredmet.selectedItem.toString()))
        var uspjesnoUpisan = false
        if(odabraniIdGrupe==null){
            onUpisError()
        }
        else {
            PredmetIGrupaListViewModel().upisiUGrupu(odabraniIdGrupe!!,
                    { upisan -> uspjesnoUpisan = upisan }, onError = ::onUpisError)
            val transaction = activity?.supportFragmentManager?.beginTransaction()
            val fragment = FragmentPoruka.newInstance()
            val bundle = Bundle()

            bundle.putString("Poruka", "Uspješno ste upisani u grupu " + odabirGrupa.selectedItem.toString() + " predmeta " + odabirPredmet.selectedItem.toString() + "!")
            fragment.arguments = bundle
            transaction?.replace(R.id.fragment_container, fragment)
            transaction?.addToBackStack(null)
            transaction?.commit()
        }
    }

}