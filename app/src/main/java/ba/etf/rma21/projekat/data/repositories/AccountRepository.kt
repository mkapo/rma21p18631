package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Account
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Appendable
import java.net.ContentHandler
import java.net.MalformedURLException

class AccountRepository {
    companion object {
        init {

        }
        private lateinit var context : Context
        fun setContext (contextApp: Context){
            context = contextApp
        }

        var acHash = "8bf62d2f-d196-4044-a824-c4ab52b2353f"
        suspend fun postaviHash(acHasha: String): Boolean {
            return withContext(Dispatchers.IO) {
                if(!acHasha.equals(acHash)){
                    val db = AppDatabase.getInstance(context)
                    db.accountDao().insertAccount(Account(1,acHasha,null))
                    db.pitanjeDao().deleteAll()
                    db.odgovorDao().deleteAll()
                    db.kvizDao().deleteAll()
                    db.grupaDao().deleteAll()
                    db.predmetDao().deleteAll()
                }

                if (acHash == null) {
                    return@withContext false
                }
                acHash = acHasha
                return@withContext true
            }
        }

        suspend fun getHash(): String {
            return withContext(Dispatchers.IO) {
                return@withContext acHash
            }
        }
    }
}