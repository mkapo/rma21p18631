package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import ba.etf.rma21.projekat.data.dao.DateConverter
import java.util.*

@Entity
@TypeConverters(DateConverter::class)
data class Account(
        @PrimaryKey var id: Int,
        @ColumnInfo (name = "acHash") var acHash: String,
        @ColumnInfo (name = "lastUpdate") var lastUpdate: Date?
)
