package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Odgovor

@Dao
interface OdgovorDao {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insertOdgovor(odgovor : Odgovor)
    @Query("SELECT * FROM Odgovor")
    suspend fun getAllOdgovor() : List<Odgovor>
    @Query("SELECT * FROM Odgovor o WHERE o.KvizId = :idKviza")
    suspend fun getOdgovorsWithKviz(idKviza : Int) : List<Odgovor>
    @Query("SELECT * FROM Odgovor o WHERE o.PitanjeId = :idPitanja")
    suspend fun getOdgovorsWithPitanje(idPitanja : Int) : List<Odgovor>
    @Query("DELETE FROM Odgovor WHERE kvizId = :idKviza")
    suspend fun deleteOdgovorWithKviz(idKviza: Int)
    @Query("SELECT * FROM Odgovor o WHERE o.PitanjeId = :idPitanja AND o.KvizId = :idKviza")
    suspend fun getOdgovorWithKvizAndPitanje(idPitanja: Int, idKviza : Int) : List<Odgovor>
    @Query("DELETE FROM Odgovor WHERE 1=1")
    suspend fun deleteAll()
}