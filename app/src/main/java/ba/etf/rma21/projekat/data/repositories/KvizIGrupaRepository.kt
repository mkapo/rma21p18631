package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.api.ApiAdapter
import ba.etf.rma21.projekat.data.models.Grupa
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class KvizIGrupaRepository {
    companion object {
        init {
            // TODO: Implementirati
        }

        suspend fun getGroupsWithKviz (idKviza : Int) : List<Grupa>{
            return withContext(Dispatchers.IO){
                val grupe = ApiAdapter.retrofit.getGroupsWithKviz(idKviza).body()
                return@withContext grupe

            }!!
        }

    }
}