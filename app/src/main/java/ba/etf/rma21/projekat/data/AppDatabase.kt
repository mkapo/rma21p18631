package ba.etf.rma21.projekat.data

import android.content.Context
import androidx.annotation.NonNull
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import ba.etf.rma21.projekat.data.dao.*
import ba.etf.rma21.projekat.data.models.*

@Database(
    entities = arrayOf(
        Grupa::class,
        Kviz::class,
        Pitanje::class,
        Predmet::class,
        Account::class,
        Odgovor::class), version = 33)
@TypeConverters(ListConverter::class)

abstract class AppDatabase : RoomDatabase() {
    abstract fun kvizDao(): KvizDao
    abstract fun pitanjeDao(): PitanjeDao
    abstract fun grupaDao(): GrupaDao
    abstract fun predmetDao(): PredmetDao
    abstract fun accountDao(): AccountDao
    abstract fun odgovorDao(): OdgovorDao
    companion object {
        private var INSTANCE: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = buildRoomDB(context)
                }
            }
            return INSTANCE!!
        }
        private fun buildRoomDB(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "RMA21DB"
            ).fallbackToDestructiveMigration().build()

        fun setInstance(db: AppDatabase) : Any {
            INSTANCE = db
            return 0
        }
    }
}