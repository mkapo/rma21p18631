package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Grupa

@Dao
interface GrupaDao {
    @Update
    suspend fun updateGroup(grupe : List<Grupa>);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertGroup(grupe : List<Grupa>)
    @Delete
    suspend fun deleteGroup(grupe : List<Grupa>)
    @Query ("SELECT * FROM Grupa")
    suspend fun getAllGroup() : List<Grupa>
    @Query("SELECT * FROM Grupa g WHERE g.id = :idKviza")
    suspend fun getGroupsWithKviz(idKviza : Int) : List<Grupa>
    @Query("DELETE FROM Grupa WHERE 1=1")
    suspend fun deleteAll()

}