package ba.etf.rma21.projekat.data.viewmodels

import android.content.Context
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.DBRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import kotlinx.coroutines.*

class PredmetIGrupaListViewModel{
    val scope = CoroutineScope(
            Job() + Dispatchers.Main)


    fun getGroupsByPredmet(idPredmeta:Int, onSuccess : (pitanja : List<Grupa>) -> Unit,
                          onError : () -> Unit){
        scope.launch {
            val result = PredmetIGrupaRepository.getGroupsByPredmet(idPredmeta)
            when (result) {
                is List<Grupa> -> onSuccess?.invoke(result)
                else -> onError?.invoke()
            }
        }
    }
    fun upisiUGrupu(idGrupa:Int, onSuccess : (pitanja : Boolean) -> Unit,
                    onError : () -> Unit) {
        scope.launch {
            val result = PredmetIGrupaRepository.upisiUGrupu(idGrupa)
            when (result) {
                is Boolean -> onSuccess?.invoke(result)
                else -> onError?.invoke()
            }
        }
    }

    suspend fun getPredmetsByGodina (godina : Int, onSuccess: (predmeti : List<Predmet>) -> Unit,
                            onError: () -> Unit){
        scope.launch {
            val result = PredmetIGrupaRepository.getPredmetsByGodina(godina)
            when(result){

                is List<Predmet> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }
}